import os
import time
import pathlib
from datetime import date
from typing import AnyStr

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

folder_to_track = "/home/user/Downloads"
folder_to_move = "/home/user/MovedFromDownloads/"


def move(curr_item_path: AnyStr, new_item_path: AnyStr, item_name: AnyStr):
    """

    :param curr_item_path: str, the path to the folder where the file is at the moment
    :param new_item_path: str, the path to the folder where to move the file
    :param item_name: str, name of the file
    :return:
    """
    if not os.path.isdir(new_item_path):
        pathlib.Path(new_item_path).mkdir(parents=True, exist_ok=True)

    print('Moved: from {} to {}'.format(curr_item_path, new_item_path))
    os.rename(curr_item_path + "/" + item_name, new_item_path + "/" + item_name)


class Mover(FileSystemEventHandler):
    def on_modified(self, event):
        for item in os.listdir(folder_to_track):
            if os.path.isdir(folder_to_track + '/' + item):
                continue

            curr_item_path = folder_to_track
            # fetch a file extension
            curr_item_extension = pathlib.Path(item).suffix
            # create new file path including folders with dates
            # new file path will be: folder_to_move + today's date + file extension
            new_item_path = folder_to_move + date.today().strftime("%b-%d-%Y") + '/' + curr_item_extension
            move(curr_item_path, new_item_path, item)


# run handler and observer
event_handler = Mover()
observer = Observer()
observer.schedule(event_handler, folder_to_track, recursive=True)
observer.start()

# stop observer if keyboard interrupt occurred (Ctrl + Z, Ctrl + C)
try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    observer.stop()

observer.join()
